library(parallel)
library(Hmisc)
load('sample.RData')
source('functions.R')

dirt = "/home/data/NYCTaxis"
fare = system(sprintf("ls -d %s/trip_fare*", dirt), intern = TRUE)
trip_data = system(sprintf("ls -d %s/trip_data*", dirt), intern = TRUE)

cl = makeCluster(12)

clusterExport(cl, c('index', 'readCol', 'fare', 'trip_data'))


total = unlist(parLapply(cl, 1:12, function(x) {(readCol(fare[x], 'total_amount') - 
                                                     readCol(fare[x], 'tolls_amount'))[index[[x]]]}))
tsec = unlist(parLapply(cl, 1:12, function(x) {readCol(trip_data[x], 'trip_time_in_secs')[index[[x]]]}))



n = 1e6
b = round(n^0.6)



rsmp_b = lapply(1:12, function(x) {sample(1:n, b)})

total_b = lapply(rsmp_b, function(x) {total[x]})
tsec_b = lapply(rsmp_b, function(x) {tsec[x]})
qt = seq(0.1, 0.9, 0.1)

clusterExport(cl, c('total_b', 'tsec_b', 'qt'))

###calculate the sd of quantiles
clusterCall(cl, library, "Hmisc", character.only = TRUE)
quantile_blb = parLapply(cl, total_b,
                         function(x) {sapply(1:100,
                                             function(y) {tmp = table(sample(x, 1e6, replace = TRUE));
                                                          wtd.quantile(as.numeric(names(tmp)), weights = tmp, probs = qt)})})
quantile_blb[[1]]
is.matrix(quantile_blb[[1]])

clusterExport(cl, 'quantile_blb')

quantile_sd = parSapply(cl, quantile_blb,
                        function(x) {apply(x, 1, function(y) {sd(y)})})

rowSums(quantile_sd)/12
##do something with quantile_sd

##calculate the sd of betas
beta_blb = parLapply(cl, 1:12,
                     function(x) {sapply(1:100,
                                         function(y) {idx = sample(1:length(total_b[[x]]), 1e6, replace = TRUE);
                                                      bt1 = cov(total_b[[x]][idx], tsec_b[[x]][idx])/var(tsec_b[[x]][idx]);
                                                      c(bt1, mean(total_b[[x]][idx]) - bt1*mean(tsec_b[[x]][idx]))})})
clusterExport(cl, 'beta_blb')
beta_blb[[1]]

beta_sd = parSapply(cl, beta_blb,
                    function(x) {apply(x, 1, function(y) {sd(y)})})

rowSums(beta_sd)/12











