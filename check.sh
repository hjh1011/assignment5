#!/bin/bash

echo "Result:" > check_result
for i in {1..12}; do
  cut -d, -f1 /home/data/NYCTaxis/trip_data_$i.csv > data_tmp;
  cut -d, -f1 /home/data/NYCTaxis/trip_fare_$i.csv > fare_tmp;
  
  diff data_tmp fare_tmp | wc -l >> check_result

  rm -f data_tmp fare_tmp 
done

